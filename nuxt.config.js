module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'LazyREST',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'lazyrestapi for lazy people' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ],
    script:[
      {src:'https://sdk.accountkit.com/id_ID/sdk.js'}
    ]
  },
  css:[
    {src:'element-ui/lib/theme-default/index.css',lang:'css'}
  ],
  mode: 'spa',
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor:[
      'axios',
      'js-cookie',
      'vuex-persistedstate'
    ]
    /*
    ** Run ESLint on save

    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
    */
  },

  plugins:[
    '~plugins/element-ui',
    '~plugins/moment',
    '~plugins/clipboards'
  ]
}
