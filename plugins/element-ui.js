/**
 * Created by muharizals on 19/11/2017.
 */
import Vue from 'vue'
import ElementUi from 'element-ui'
const locale = require('element-ui/lib/locale/lang/en')

Vue.use(ElementUi,{locale})
