/**
 * Created by muharizals on 19/11/2017.
 */
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'
import {BASE_URL,GRAPH_FB,FB_KIT} from './config.env'

export const state = () =>({
  user:null,
  authenticated:false
})

export const plugins = [createPersistedState({
  storage: {
    getItem: key => Cookies.get(key),
    setItem: (key, value) => Cookies.set(key, value, { expires:1, secure: false }),
    removeItem: key => Cookies.remove(key)
  }
})]

export const mutations = {
  SET_USER: (state, user)=> {
    state.user = user
  },
  SET_AUTH: (state,auth)=> {
    state.authenticated = auth
  }
}

export const getters = {
  isAuthenticated (state) {
    return state.authenticated
  },
  user(state){
    return state.user
  }
}

export const actions = {
  async checkUser({commit},{code}){
    try{
      const {data} = await axios.get(`${GRAPH_FB}${FB_KIT.version}/access_token?grant_type=authorization_code&code=${code}&access_token=${FB_KIT.access_token}`)
      const response = await axios.get(`${GRAPH_FB}${FB_KIT.version}/me?access_token=${data.access_token}`);
      return response.data;
    }catch (error){
      throw error
    }
  },
  async login({commit},body){
    try {
      const {data}  = await axios.post(`${BASE_URL}login`,body)
      if(data){
        commit('SET_USER',data)
        commit('SET_AUTH',true)
      }
      else{
        throw {message:"Terjadi kesalahan tidak diketahui"}
      }
    }catch(error){
      throw error
    }
  },
  logOut({commit}){
    commit('SET_USER',null)
    commit('SET_AUTH',false)
  }
}
